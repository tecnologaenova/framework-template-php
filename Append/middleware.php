<?php

/** @var \Slim\App $app */

$app = \Enova\Core\Utils\Registry::get("slim.app");
$container = \Enova\Core\Utils\Registry::get("container");

$app->add(new Tuupola\Middleware\CorsMiddleware([
    "origin" => ["*"],
    "methods" => ["GET", "POST", "PUT", "PATCH", "DELETE"],
    "headers.allow" => [],
    "headers.expose" => [],
    "credentials" => false,
    "cache" => 0,
]));

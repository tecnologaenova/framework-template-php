<?php

$container = \Enova\Core\Utils\Registry::get("container");

use Enova\Dependency\Logger as LoggerDependency;
use Enova\Dependency\Database as DatabaseDependency;
use Enova\Dependency\Cache as CacheDependency;
use Enova\Utils\Commons\HTTPStatusCode;

$container['logger'] = (new LoggerDependency())($container);

$container['database']=(new DatabaseDependency())($container);

$container['cache']=(new CacheDependency())($container);

$container['notFoundHandler'] = function ($container) {
    return function ($request, $response) use ($container) {
        $contentType = "application/json";
        if (array_key_exists("HTTP_ACCEPT", $request->getHeaders())) {
            $contentType = $request->getHeaders()["HTTP_ACCEPT"];
        }
        if (array_key_exists("HTTP_CONTENT_TYPE", $request->getHeaders())) {
            $contentType = $request->getHeaders()["HTTP_CONTENT_TYPE"];
        }
        if(is_array($contentType)){
            $contentType = "application/json";
        }
        return $container['response']
                        ->withStatus(HTTPStatusCode::NOT_FOUND)
                        ->withHeader('Content-Type', $contentType)
                        ->write(json_encode(array('message' => HTTPStatusCode::convertStatusCodeToString(HTTPStatusCode::NOT_FOUND))));
    };
};
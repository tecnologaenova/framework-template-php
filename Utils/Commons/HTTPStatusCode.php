<?php

namespace Enova\Utils\Commons;

/**
 * Clase que contiene los codigo http
 * @author Antonio Shilon
 */
class HTTPStatusCode {
    /**
     * se utiliza principalmente cuando se divide una accion en varias peticiones
     * con este se confirma que se recibio la peticion y se esperan las siguientes
     */
    const REQUEST_CONTINUE = 100;
    /**
     * la petición finalizó de forma correcta
     */
    const OK = 200;
    /**
     * el recurso fue creado
     */
    const CREATED = 201;
    /**
     * la petición fue aceptada pero el procesamiento de la misma no ha sido finalizada
     */    
    const ACCEPTED = 202;
    /**
     * el procesamiento de la petición fue exitoso pero no se tiene información en el body
     */    
    const NO_CONTENT = 204;
    /**
     * es cuando la respuesta alberga una respuesta parcial, por ejemplo se pide un trozo de
     */    
    const PARTIAL_CONTENT = 206;
    /**
     * el recurso fue movido permanentemente el servidor encontró el recurso solicitado
     * pero fue movida a otra dirección de forma permanente
     */
    const MOVED_PERMANENTLY = 301;
    /**
     * el recurso fue encotnrado temporalmente en otra URI, la redirección puede ser alterada
     * en ocasiones, este código informa que URI es la que debe consumir el cliente en futuras
     */    
    const FOUND = 302;
    /**
     * se utilizan para las peticiones de tipo GET, estas peticiones evaluan si el recurso ha sido
     * modificado anteriormente, sino se entrega este valor con body vacío
     */
    const NOT_MODIFIED = 304;
    /**
     * se tiene una petición con una sintaxis incorrecta.
     */
    const BAD_REQUEST = 400;
    /**
     * el request requiere autentificación, no tiene la cabecera Authorizationn en los headers
     * Este código deberá usarse aunque solo sea este por Basic Auth
     */    
    const UNAUTHORIZED = 401;
    /**
     * código reservado para uso futuro, pero se emplea en algunos sistemas para indicar que
     * la autentificación validó que el pago de la membresía o del servicio no ha sido cubierto
     * podría tener acceso a la librería el cliente de la petición pero por falta de pago se niega el servicio
     */    
    const PAYMENT_REQUIRED = 402;
    /**
     * El servidor entendió la solicitud pero se niega a cumplirla, en este caso la autorización
     * no sirve, el servidor debe comentar el porque de la denegación del servicio, si no se desea
     * exponer el porque no se cumplió la petición se deberá regresar 404  NOT FOUND
     */    
    const FORBIDDEN = 403;
    /**
     * El servidor no encontró el recurso solicitado, es decir, no hizo match con ninguna URI de las disponibles
     * No se indica si el recurso no se encuentra de forma temporal o permanente, si el recurso fue eliminado y no volverá
     * deberá utilizarse el código 401 GONE, este estatus se utiliza cuando el servidor no desea revelar exactamente
     * porqué el request fue rechazado.
     */    
    const NOT_FOUND = 404;
    /**
     * El método especificado para la URI no corresponde y no está permitida, el response deberá incluir un header Allow done contenga
     * los métodos disponibles para esa petición.
     */
    const METHOD_NOT_ALLOWED = 405;
    /**
     * este es afectado por los headers Accept-Charset y Accept-Language, si el servidor no soporta el charset o el lenguaje solicitado
     * devolverá este código de error
     */
    const NOT_ACCEPTABLE = 406;
    /**
     * este código es similar a 401 (Unauthorized) solo que este indica por credenciales de autentificación para
     * un servidor proxy que se encuentre entre el navegador y el servidor
     */    
    const PROXY_AUTHENTIFICATION_REQUIRED = 407;
    /**
     * indica que el servidor entiende el tipo de contenido de la entidad de solicitud
     */
    const UNPROCESSABLE_ENTITY = 422;
    
    public static function convertStatusCodeToString($code) {
        $reflection = new \ReflectionClass(new HTTPStatusCode());
        $constants = $reflection->getConstants();
        $key = array_search($code, $constants);
        return ucwords(strtolower(str_replace(["_"], [" "], $key)));
    }
}
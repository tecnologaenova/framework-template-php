<?php
namespace Enova\Utils\Commons\Db;

use Illuminate\Database\Capsule\Manager;
use Phinx\Migration\AbstractMigration;


class Migration extends AbstractMigration
{
    /** @var type manager */
    public $manager = null;
    /** @var type schema */
    public $schema = null;

    /**
     * init method
     * Contiene la implementación con eloquent para el sistema de migraciones
     */
    public function init()
    {
        (new \Dotenv\Dotenv(dirname(dirname(dirname(__DIR__)))))->load();
        $this->manager = new Manager();

        $this->manager->addConnection([
            'driver'    => getenv('PHINX_DRIVER'),
            'host'      => getenv('PHINX_HOSTNAME'),
            'database'  => getenv('PHINX_DATABASE'),
            'username'  => getenv('PHINX_USERNAME_DB'),
            'password'  => getenv('PHINX_PASSWORD_DB'),
            'charset'   => getenv('UTF8', 'utf8'),
            'collation' => getenv('UTF8_UNICODE'),
            'prefix'    => getenv('DB_PREFIX', ''),
        ]);

        $this->manager->setAsGlobal();
        $this->manager->bootEloquent();
        $this->schema = $this->manager->schema();
    }
}
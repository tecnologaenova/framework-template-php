<?php
require 'vendor/autoload.php';
(new \Dotenv\Dotenv(__DIR__))->load();

return [
    "paths" => [
        "migrations" => __DIR__."/Migrations",
        "seeds" => __DIR__."/Seeders"
    ],
    "migration_base_class" => "Enova\Utils\Commons\Db\Migration",
    "environments" => [
        "default_migration_table" => "migrations",
        "default_database" => "production",
        "production" => [
            "adapter" => getenv('PHINX_DRIVER'),
            "host" => getenv("PHINX_HOSTNAME"),
            "name" => getenv("PHINX_DATABASE"),
            "user" => getenv("PHINX_USERNAME_DB"),
            "pass" => getenv("PHINX_PASSWORD_DB"),
            "port" => getenv("PHINX_PORT"),
            "charset" => "utf8",
        ]
    ],
    "version_order" => "creation"
];
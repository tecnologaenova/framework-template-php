<?php

namespace Enova\Dependency;

/**
 * Handler for NotFound response in the case if Slim doesn't know the uri
 */
class NotFoundHandler {

    /**
     * Invoke method
     * @param Container $container Slim container
     * @return Response slim response
     */
    public function __invoke($container) {
        $request = $container->get("request");
        $response = $container->get("response");
        return $this->handler($request, $response, $container);
    }

    /**
     * Handler for response
     * @param Request $request Slim's object Request
     * @param Response $response Slim's object Response
     * @param Container $container Slim's object Container
     * @return Response
     */
    public function handler($request, $response, $container) {
        $contentType = "application/json";
        if (array_key_exists("HTTP_ACCEPT", $request->getHeaders())) {
            $contentType = $request->getHeaders()["HTTP_ACCEPT"];
        }
        if (array_key_exists("HTTP_CONTENT_TYPE", $request->getHeaders())) {
            $contentType = $request->getHeaders()["HTTP_CONTENT_TYPE"];
        }
        if (is_array($contentType)) {
            $contentType = "application/json";
        }
        return $container['response']
                        ->withStatus(404)
                        ->withHeader('Content-Type', $contentType)
                        ->write(json_encode(array('message' => 'Not found.')));
    }

}

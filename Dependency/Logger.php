<?php

namespace Enova\Dependency;

use Enova\Factory\LoggerFactory;

/**
 * Class for create the cache dependency
 */
class Logger {

    /**
     * Invoke method
     * @param Container $container slim container
     * @return \Monolog\Logger 
     */
    public function __invoke($container) {
        $settings = $container->get('settings')['logger'];
        $loggerFactory = new LoggerFactory($settings);
        return $loggerFactory->getLogger($settings['type']);;
    }

}

<?php

namespace Enova\Dependency;

use Enova\Factory\CacheFactory;

/**
 * Class for create the cache dependency
 */
class Cache {

    /**
     * Invoke method for create the cache
     * @param Container $container Slim's Container
     * @return Symfony\Component\Cache\Simple\AbstractCache object with this base class
     */
    public function __invoke($container) {
        $settings = $container->get('settings')['cache'];
        $cacheFactory = new CacheFactory($settings);
        $cache = $cacheFactory->getCache($settings['type']);
        return $cache;
    }

}

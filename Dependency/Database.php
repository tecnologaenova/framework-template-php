<?php

namespace Enova\Dependency;

use Enova\Factory\DatabaseFactory;
/**
 * Class for create the dependency for database
 */
class Database {

    /**
     * Invoke method for create the database
     * @param Cntainer $container Slim's container
     * @return \Factory\DatabaseFactory
     */
    public function __invoke($container) {
        $databaseFactory = new DatabaseFactory(['default', 'calendar']);
        return $databaseFactory;
    }

}

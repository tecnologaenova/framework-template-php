<?php
namespace Enova\Validator;

use Factory\ValidatorFactory;
use Illuminate\Database\Eloquent\Model;

abstract class BaseValidator {

    public $entity;
    protected $data;
    protected $errors;

    /**
     * Constructor Method
     * @param Model $entity
     * @param array $data
     */
    public function __construct(Model $entity, array $data) {
        $this->entity = $entity;
        $this->data = $data;
    }

    /**
     * Method to define validation rules
     * @abstract
     */
    abstract public function getRules();

    /**
     * Method to validate data for entity
     * @return boolean
     */
    public function isValid() {
        $rules = $this->getRules();
        $validacion = (new ValidatorFactory)->make($this->data, $rules);
        $isValid = $validacion->passes();
        $this->errors = $validacion->messages();
        return $isValid;
    }

    /**
     * Method to transform or parse data from request
     * @param array $data
     * @return array
     */
    public function prepareData(array $data) {
        return $data;
    }

    /**
     * Method to persist data
     * @return boolean
     */
    public function save() {
        if (!$this->isValid()) {
            return false;
        }
        $this->entity->fill($this->prepareData($this->data));
        $this->entity->save();
        return true;
    }

    /**
     * Getter method to retrieve errors attribute
     * @return array
     */
    public function getErrors() {
        return $this->errors;
    }

    /**
     * Getter method to retrieve entity attribute
     * @return array
     */
    public function getEntity() {
        return  $this->entity;
    }
}
<?php

session_start();
const APP_ROOT = __DIR__ . "/..";
$root = APP_ROOT;
$composer = require APP_ROOT . "/vendor/autoload.php";

use Enova\Core\Utils\Registry;
use Enova\Router\Implementation\RouterApp;

(new \Dotenv\Dotenv(APP_ROOT))->load();

Registry::set("slim.path", $root);

// Instantiate the app
$settings = require __DIR__ . '/../config/app.php';
$app = new \Slim\App($settings);

$router = new RouterApp($composer);


Registry::set("slim.app", $app);

/** @var \DI\Container $c */
$c = $app->getContainer();

Registry::set("container", $c);


require APP_ROOT . "/Append/dependencies.php";

require APP_ROOT . "/Append/middleware.php";

require APP_ROOT . '/storage/routes/routes.php';

$database =$c->get('database');

$database->boot();

$app->run();

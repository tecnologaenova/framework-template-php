<?php

namespace Modules\ModuloA\Controller;

use Enova\Router\Implementation\Controller;
use Slim\Http\Request;
use Slim\Http\Response;
use Enova\Utils\Commons\HTTPStatusCode;

/**
 * @BaseRoute("/v1/microservice")
 */
class Ejemplo extends Controller {

    /**
     * @Route("/greeting/{name:[\w]+}", methods="GET,POST", name="home.hello")
     */
    public function helloAction(Request $request, Response $response, $args)  {
        $cache = $this->container->get("cache");

        if (!$cache->has("hola")) {
            $cache->set("hola", json_encode(["hola" => 'mundo']));
            $dato = json_encode(["hola" => 'mundo']);
        } else {
            $dato = $cache->get("hola");
        }

        return $response->withJson([
            "cache"=>$dato,
            "url_param"=>$args['name'],
            "param_tag" => $request->getParam("tag"), 
            "body" => $request->getParsedBody(), 
            "request" => $request->getQueryParams()
        ], HTTPStatusCode::OK);
    }

}

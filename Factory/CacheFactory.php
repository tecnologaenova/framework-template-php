<?php

namespace Enova\Factory;

use Symfony\Component\Cache\Simple\FilesystemCache;

/**
 * Factory for caching
 */
class CacheFactory {

    private $settings;

    /**
     * Constructor method
     * @param array $settings configuration
     */
    public function __construct($settings) {
        $this->settings = $settings;
    }

    /**
     * Get specific cache from factory
     * @param string $cacheType
     * @return FilesystemCache
     */
    public function getCache(string $cacheType) {
        $cache = null;
        switch ($cacheType) {
            case'File':
                $cache = new FilesystemCache('', $this->settings['lifetime'], $this->settings['path']);
                break;
        }
        return $cache;
    }

}

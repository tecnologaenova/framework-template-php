<?php

namespace Enova\Factory;

use Monolog\Handler\StreamHandler;
use Monolog\Logger as MonologLogger;
use Monolog\Processor\UidProcessor;

/**
 * Factory for logs
 */
class LoggerFactory {

    private $settings;

    /**
     * Constructor method
     * @param array $settings configuration
     */
    public function __construct($settings) {
        $this->settings = $settings;
    }

    /**
     * Factory method
     * @param string $loggerType logger type for the factory
     * @return \Monolog\Logger
     */
    public function getLogger(string $loggerType) {
        $logger = null;
        switch ($loggerType) {
            case'Monolog':
                $logger = new MonologLogger($this->settings['name']);
                $logger->pushProcessor(new UidProcessor);
                $logger->pushHandler(new StreamHandler(
                        $this->settings['path'], getenv("DEBUG") ? MonologLogger::DEBUG : $this->settings['level']
                ));
                break;
        }

        return $logger;
    }

}

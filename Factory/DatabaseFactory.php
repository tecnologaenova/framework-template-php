<?php

namespace Enova\Factory;

use Illuminate\Database\Capsule\Manager;

/**
 * Factory for the database
 */
class DatabaseFactory {
    
    private $connections;
    
    /**
     * Constructor method
     * @param array|string $connections different connections for database
     */
    public function __construct($connections = 'default'){
        if(!is_array($connections)){
            $this->connections=[$connections];
        }
        $this->connections = $connections;
    }
    
    /**
     * Booting Eloquent method
     */
    public function boot() {
        $capsule = new Manager();
        if(is_array($this->connections)){
            foreach ($this->connections as $name){
                $parameters = $this->getConnectionParameters($name);
                $capsule->addConnection($parameters,$name);
            }
        } else {
            $parameters = $this->getConnectionParameters($this->connections);
            $capsule->addConnection($parameters);
        }
        $capsule->setAsGlobal();
        $capsule->bootEloquent();
    }

    /**
     * Get connection parameters from environment variables
     * @param string $connectionName
     * @return array connection values
     */
    private function getConnectionParameters($connectionName) {
        
        if($connectionName == 'default'){
            $connectionName = '';
        } else {
            $connectionName.='_';
        }
        
        return [
            "driver" => getenv(strtoupper($connectionName)."DB_DRIVER"),
            "host" => getenv(strtoupper($connectionName)."DB_HOSTNAME"),
            "port" => getenv(strtoupper($connectionName)."DB_PORT"),
            "database" => getenv(strtoupper($connectionName)."DB_DATABASE"),
            "username" => getenv(strtoupper($connectionName)."DB_USERNAME"),
            "password" => getenv(strtoupper($connectionName)."DB_PASSWORD"),
            "charset" => getenv("UTF8") ?? "utf8",
            "collation" => getenv("DB_COLLATION") ?? "utf8_unicode_ci",
        ];
    }
}

<?php
// application.php

$composer = require __DIR__.'/vendor/autoload.php';

use Enova\Core\Implementation\CommandApplication;

$directory = __DIR__;

\Enova\Core\Utils\Registry::set("root.directory", $directory);

$application = new CommandApplication($composer,["Enova"]);

$application->run();

<?php

return [
    'settings' => [
        'displayErrorDetails' => false, // set to false in production
        'addContentLengthHeader' => true, // Allow the web server to send the content-length header
        'determineRouteBeforeAppMiddleware' => true,
        'displayErrorDetails' => getenv("DEBUG"),
        // Monolog settings
        'logger' => [
            'type'=>getenv('LOG_TYPE'),
            'name' => getenv("LOG_NAME"),
            'path' => APP_ROOT . "/storage/logs/app.log",
            'level' => strtoupper((new ReflectionClass("\Monolog\Logger"))->getConstant(getenv("LOG_LEVEL")))
        ],
        "cache" => [
            "type" => getenv('CACHE_TYPE'),
            "path" => APP_ROOT . "/storage/cache/files/",
            "lifetime"=>getenv('CACHE_LIFETIME')
        ]
    ],
];
<?php

namespace Enova\Exception;

use Interop\Container\ContainerInterface;
use DI\Container;
use Slim\Http\Request;
use Slim\Http\Response;

abstract class BaseException extends \Exception {

    protected $status_code = 500;
    protected $default_message;

    /**
     * @param string $message
     * @param int $code
     * @param \Throwable|\Exception|null $previous
     */
    public function __construct($message = "", $code = 0, $previous = null) {
        if (empty($message) && !empty($this->default_message)) {
            $message = $this->default_message;
        }
        parent::__construct($message, $code, $previous);
    }

    public function getParams() {
        return [ "message" => $this->message ? : get_class($this)];
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param ContainerInterface $container
     * @return Response
     */
    public function render(Request $request, Response $response, Container $container) {
        $response = $response->withStatus($this->status_code);
            return $response->withJson($this->getParams());
        
    }

}
